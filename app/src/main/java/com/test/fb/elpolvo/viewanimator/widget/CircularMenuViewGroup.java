package com.test.fb.elpolvo.viewanimator.widget;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.content.Context;
import android.os.Build;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPropertyAnimatorCompat;
import android.support.v4.view.ViewPropertyAnimatorListener;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.test.fb.elpolvo.viewanimator.R;
import com.test.fb.elpolvo.viewanimator.animation.Animations;
import com.test.fb.elpolvo.viewanimator.model.MenuItemContainer;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by elpolvo on 7/28/16.
 */
public class CircularMenuViewGroup extends LinearLayout {
    // ===========================================================
    // Constants
    // ===========================================================
    private static final float INIT_ROTATE_VALUE = 0f;
    private static final float PRESSED_ROTATE_VALUE = 45f;
    private static final long ROTATE_DURATION = 200L;
    private static final long REVEAL_DURATION = 250L;
    private static final long ANIMATION_DURATION = 50L;
    // ===========================================================
    // Fields
    // ===========================================================
    @BindView(R.id.menu_background)
    protected FrameLayout menuBackground;
    @BindView(R.id.fab_menu)
    protected FloatingActionButton fabMenu;
    @BindView(R.id.menu_item_container)
    protected LinearLayout menuItemContainer;
    private boolean isMenuDisplayed = false;
    // ===========================================================
    // Constructors
    // ===========================================================
    public CircularMenuViewGroup(Context context) {
        this(context, null);
    }

    public CircularMenuViewGroup(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CircularMenuViewGroup(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View view = inflater.inflate(R.layout.circular_menu_view_group, this, true);

        ButterKnife.bind(this, view);

        menuBackground.setVisibility(GONE);

        fabMenu.setFocusable(true);
        fabMenu.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                showHideMenu();
            }
        });
    }
    // ===========================================================
    // Getter & Setter
    // ===========================================================
    private int[] getFabLocation() {
        final int[] location = new int[2];
        fabMenu.getLocationInWindow(location);
        return location;
    }
    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    // ===========================================================
    // Methods
    // ===========================================================
    public void createMenu(final ArrayList<MenuItemContainer> menuItems) {
        menuItemContainer.setOrientation(VERTICAL);
        for (final MenuItemContainer menuItem : menuItems) {
            final MenuItemView menuItemView = new MenuItemView(getContext(), menuItem);
            final LayoutParams lp = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lp.gravity = Gravity.END;
            menuItemView.setLayoutParams(lp);
            menuItemView.setVisibility(GONE);
            menuItemContainer.addView(menuItemView);
        }
    }

    private void toggleMenuVisibilityFlag() {
        isMenuDisplayed = !isMenuDisplayed;
    }

    private void showHideMenu() {
        if (!isMenuDisplayed) {
            showMenu();
        } else {
            hideMenu();
        }

        toggleMenuVisibilityFlag();
    }

    private void showMenu() {
        final float rotationValue = PRESSED_ROTATE_VALUE;
        final ViewPropertyAnimatorCompat rotateFabAnimation = Animations.getRotateAnimator(fabMenu, rotationValue, ROTATE_DURATION);
        rotateFabAnimation.setListener(new FabRotationListener(menuBackground));
        rotateFabAnimation.start();

        menuBackground.setVisibility(VISIBLE);
        // reveal background
        menuBackground.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.white_transparent));
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) { // old apis, reveal with fade only
            final float startAlpha = 0.0f;
            final float endAlpha = 1.0f;
            final Animator fadeIn = Animations.getFadeAnimator(menuBackground, startAlpha, endAlpha);
            fadeIn.addListener(new AnimatorMenuContainerListener(menuBackground));
            fadeIn.start();
        } else {
            final int[] fabLocation = getFabLocation();
            final int centerX = fabLocation[0] + (fabMenu.getWidth() / 2);
            final int centerY = fabLocation[1] - (fabMenu.getHeight() / 2);

            // get the final radius for the clipping circle
            final float initialRadius = 0f;
            final float finalRadius = (float) Math.hypot(centerX, centerY);

            final Animator reveal = Animations.getRevealAnimator(menuBackground, centerX, centerY, initialRadius, finalRadius);
            reveal.setDuration(REVEAL_DURATION);
            reveal.addListener(new AnimatorMenuContainerListener(menuBackground));
            reveal.start();
        }

        // animate items from the bottom to the top
        final long delayIncrement = 50L;
        long startDelay = 15L;
        final ArrayList<Animator> animators = new ArrayList<>();
        for (int i = menuItemContainer.getChildCount() - 1; i >= 0; i--) {
            final View item = menuItemContainer.getChildAt(i);
            final AnimatorSet animationSet = Animations.getFadeInScaleInAnimator(item, ANIMATION_DURATION);
            animationSet.setStartDelay(startDelay);
            animationSet.addListener(new AnimatorMenuItemListener(item, VISIBLE, AnimatorMenuItemListener.APPLY_ALPHA_VALUE));
            startDelay += delayIncrement;
            animators.add(animationSet);
        }
        Animations.playTogether(animators);
    }

    private void hideMenu() {
        final float rotationValue = INIT_ROTATE_VALUE;
        final ViewPropertyAnimatorCompat rotateFabAnimation = Animations.getRotateAnimator(fabMenu, rotationValue, ROTATE_DURATION);
        rotateFabAnimation.setListener(new FabRotationListener(menuBackground));
        rotateFabAnimation.start();

        // hide background
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) { // old apis, hide with fade only
            final float startAlpha = 1.0f;
            final float endAlpha = 0.0f;
            final Animator fadeOut = Animations.getFadeAnimator(menuBackground, startAlpha, endAlpha);
            fadeOut.addListener(new AnimatorMenuContainerListener(menuBackground, GONE));
            fadeOut.start();
        } else {
            final int[] fabLocation = getFabLocation();
            final int centerX = fabLocation[0];
            final int centerY = fabLocation[1] - (fabMenu.getHeight() / 2);

            // get the final radius for the clipping circle
            final float initialRadius = (float) Math.hypot(centerX, centerY);
            final float finalRadius = 0f;

            final Animator reveal = Animations.getRevealAnimator(menuBackground, centerX, centerY, initialRadius, finalRadius);
            reveal.setDuration(REVEAL_DURATION);
            reveal.addListener(new AnimatorMenuContainerListener(menuBackground, GONE));
            reveal.start();
        }

        final ArrayList<Animator> animators = new ArrayList<>();
        for (int i = 0; i < menuItemContainer.getChildCount(); i++) {
            final View item = menuItemContainer.getChildAt(i);
            final Animator fadeOut = Animations.getFadeOutScaleOutAnimator(item, ANIMATION_DURATION);
            fadeOut.addListener(new AnimatorMenuItemListener(item, GONE));
            animators.add(fadeOut);
        }
        Animations.playTogether(animators);
    }
    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
    private class FabRotationListener implements ViewPropertyAnimatorListener {
        private final View background;

        public FabRotationListener(View background) {
            this.background = background;
        }

        @Override
        public void onAnimationStart(View view) {
            view.setClickable(false);
            background.setFocusable(false);
            background.setClickable(false);
        }

        @Override
        public void onAnimationEnd(View view) {
            view.setClickable(true);
            background.setFocusable(true);
            background.setClickable(true);
        }

        @Override
        public void onAnimationCancel(View view) {

        }
    }

    private class AnimatorMenuContainerListener implements Animator.AnimatorListener {
        private final View view;
        private final int visibility;

        public AnimatorMenuContainerListener(final View view, final int visibility) {
            this.view = view;
            this.visibility = visibility;
        }

        public AnimatorMenuContainerListener(final View view) {
            this(view, View.VISIBLE);
        }

        @Override
        public void onAnimationStart(Animator animator) {
            view.setFocusable(false);
            view.setClickable(false);
            view.setOnTouchListener(new OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    return false;
                }
            });
        }

        @Override
        public void onAnimationEnd(Animator animator) {
            view.setFocusable(true);
            view.setClickable(true);
            if (visibility == GONE) {
                view.setVisibility(visibility);
            }
            view.setOnTouchListener(new OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    showHideMenu();
                    return false;
                }
            });
        }

        @Override
        public void onAnimationCancel(Animator animator) {

        }

        @Override
        public void onAnimationRepeat(Animator animator) {

        }
    }

    private class AnimatorMenuItemListener implements Animator.AnimatorListener {
        private static final float APPLY_ALPHA_VALUE = 0f;
        private static final float DEF_ALPHA_VALUE = -1f;
        private final View viewItem;
        private final int visibility;
        private final float alpha;

        public AnimatorMenuItemListener(final View viewItem, final int visibility, final float alpha) {
            this.viewItem = viewItem;
            this.visibility = visibility;
            this.alpha = alpha;
        }

        public AnimatorMenuItemListener(final View viewItem, final int visibility) {
            this(viewItem, visibility, DEF_ALPHA_VALUE);
        }

        @Override
        public void onAnimationStart(Animator animator) {
            if (alpha == APPLY_ALPHA_VALUE) {
                viewItem.setAlpha(alpha);
            }
            if (visibility == View.VISIBLE) {
                viewItem.setVisibility(visibility);
            }
        }

        @Override
        public void onAnimationEnd(Animator animator) {
            if (visibility == View.GONE) {
                viewItem.setVisibility(visibility);
            }
        }

        @Override
        public void onAnimationCancel(Animator animator) {

        }

        @Override
        public void onAnimationRepeat(Animator animator) {

        }
    }
}
