package com.test.fb.elpolvo.viewanimator;

import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.test.fb.elpolvo.viewanimator.model.MenuItemContainer;
import com.test.fb.elpolvo.viewanimator.widget.CircularMenuViewGroup;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {
    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================
    private Resources res;
    @BindView(R.id.button)
    protected Button button;
    @BindView(R.id.circular_menu)
    protected CircularMenuViewGroup circularMenu;
    // ===========================================================
    // Constructors
    // ===========================================================

    // ===========================================================
    // Getter & Setter
    // ===========================================================
    private ArrayList<MenuItemContainer> getMenuItems() {
        final MenuItemContainer device = new MenuItemContainer(res.getString(R.string.device), R.drawable.ic_action_new_picture);
        final MenuItemContainer camera = new MenuItemContainer(res.getString(R.string.camera), R.drawable.ic_action_camera);
        final ArrayList<MenuItemContainer> menuItems = new ArrayList<>();
        menuItems.add(device);
        menuItems.add(camera);
        return menuItems;
    }
    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        res = getResources();
        ButterKnife.bind(this);

        circularMenu.createMenu(getMenuItems());
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this, "click", Toast.LENGTH_SHORT).show();
            }
        });
    }
    // ===========================================================
    // Methods
    // ===========================================================

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}
