package com.test.fb.elpolvo.viewanimator.model;

/**
 * Created by elpolvo on 7/28/16.
 */
public class MenuItemContainer {
    public static final String EVENT_BUS_TAG = "MenuItemContainer";

    private static final String EMPTY_DESC = "";
    private static final int NO_ID = -1;

    private final String description;
    private final int iconId;

    public MenuItemContainer(String description, int iconId) {
        this.description = description;
        this.iconId = iconId;
    }

    public MenuItemContainer() {
        this(EMPTY_DESC, NO_ID);
    }

    public String getDescription() {
        return description;
    }

    public int getIconId() {
        return iconId;
    }

    public boolean isEmpty() {
        return description.equals(EMPTY_DESC) && iconId == NO_ID;
    }
}
