package com.test.fb.elpolvo.viewanimator.animation;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.os.Build;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorCompat;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.animation.AnimationSet;
import android.view.animation.OvershootInterpolator;

import java.util.ArrayList;

/**
 * Created by elpolvo on 7/28/16.
 */
public class Animations {
    public static ViewPropertyAnimatorCompat getRotateAnimator(final View view, final float rotationValue, final long duration) {
        final OvershootInterpolator interpolator = new OvershootInterpolator();
        return ViewCompat.animate(view).rotation(rotationValue).withLayer().setDuration(duration).setInterpolator(interpolator);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static Animator getRevealAnimator(final View view, final int centerX, final int centerY, final float initialRadius, final float finalRadius) {
        return ViewAnimationUtils.createCircularReveal(view, centerX, centerY, initialRadius, finalRadius);
    }

    public static Animator getFadeAnimator(final View view, final float startAlpha, final float endAlpha) {
        return ObjectAnimator.ofFloat(view, "alpha", startAlpha, endAlpha);
    }

    public static Animator getScaleAnimator(final View view, final String propertyName, final float startValue, final float endValue) {
        return ObjectAnimator.ofFloat(view, propertyName, startValue, endValue);
    }

    public static AnimatorSet getFadeInScaleInAnimator(final View view, final long duration) {
        // fade animation
        final float startAlpha = 0.0f;
        final float endAlpha = 1.0f;
        final Animator fadeAnimator = getFadeAnimator(view, startAlpha, endAlpha);
        // scale animation
        final float startScale = 0.2f;
        final float endScale = 1.0f;
        final Animator scaleX = getScaleAnimator(view, "scaleX", startScale, endScale);
        final Animator scaleY = getScaleAnimator(view, "scaleY", startScale, endScale);

        final AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(fadeAnimator, scaleX, scaleY);
        animatorSet.setDuration(duration);
        return animatorSet;
    }

    public static AnimatorSet getFadeOutScaleOutAnimator(final View view, final long duration) {
        // fade animation
        final float startAlpha = 1.0f;
        final float endAlpha = 0.0f;
        final Animator fadeAnimator = getFadeAnimator(view, startAlpha, endAlpha);
        // scale animation
        final float startScale = 1.0f;
        final float endScale = 0.2f;
        final Animator scaleX = getScaleAnimator(view, "scaleX", startScale, endScale);
        final Animator scaleY = getScaleAnimator(view, "scaleY", startScale, endScale);

        final AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(fadeAnimator, scaleX, scaleY);
        animatorSet.setDuration(duration);
        return animatorSet;
    }

    public static void playTogether(final ArrayList<Animator> animators) {
        final AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(animators);
        animatorSet.start();
    }
}
