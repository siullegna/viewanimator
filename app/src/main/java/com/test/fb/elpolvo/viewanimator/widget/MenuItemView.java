package com.test.fb.elpolvo.viewanimator.widget;

import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.test.fb.elpolvo.viewanimator.R;
import com.test.fb.elpolvo.viewanimator.event.EventBus;
import com.test.fb.elpolvo.viewanimator.model.MenuItemContainer;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by elpolvo on 7/28/16.
 */
public class MenuItemView extends LinearLayout implements View.OnClickListener {
    // ===========================================================
    // Constants
    // ===========================================================
    private final MenuItemContainer menuItemContainer;
    // ===========================================================
    // Fields
    // ===========================================================
    @BindView(R.id.description_id)
    protected TextView descriptionId;
    @BindView(R.id.fab_id)
    protected FloatingActionButton fabId;
    // ===========================================================
    // Constructors
    // ===========================================================
    public MenuItemView(Context context) {
        this(context, new MenuItemContainer());
    }

    public MenuItemView(final Context context, final MenuItemContainer menuItemContainer) {
        this(context, null, menuItemContainer);
    }

    public MenuItemView(Context context, AttributeSet attrs, final MenuItemContainer menuItemContainer) {
        this(context, attrs, 0, menuItemContainer);
    }

    public MenuItemView(Context context, AttributeSet attrs, int defStyleAttr, final MenuItemContainer menuItemContainer) {
        super(context, attrs, defStyleAttr);

        final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View view = inflater.inflate(R.layout.menu_item_view, this, true);

        ButterKnife.bind(this, view);

        if (!menuItemContainer.isEmpty()) {
            descriptionId.setText(menuItemContainer.getDescription());
            fabId.setImageResource(menuItemContainer.getIconId());
        }

        this.menuItemContainer = menuItemContainer;
        setOnClickListener(this);
    }
    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================
    @Override
    public void onClick(View v) {
        EventBus.INSTANCE.getBus().post(MenuItemContainer.EVENT_BUS_TAG, menuItemContainer);
    }
    // ===========================================================
    // Methods
    // ===========================================================

    // ===========================================================
    // Inner and Anonymous Classes
    // ===========================================================
}
