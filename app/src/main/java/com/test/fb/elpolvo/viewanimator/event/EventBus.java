package com.test.fb.elpolvo.viewanimator.event;

import com.hwangjr.rxbus.Bus;
import com.hwangjr.rxbus.RxBus;

/**
 * Created by elpolvo on 7/28/16.
 */
public enum EventBus {
    INSTANCE;

    private final Bus bus = RxBus.get();

    public Bus getBus() {
        return bus;
    }
}
